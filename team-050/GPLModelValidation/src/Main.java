public class Main {
	public static void main(String[] args) {
		String file;

		try {
			// Takes a file by name
			String fileName = args[0];
			String[] fileExtension = fileName.split("\\.");

			if (fileExtension.length < 2) {
				fileName += ".xmi";
			}

			file = fileName;
		} catch (Exception e) {
			// By default
			file = "instances/Agenda.xmi";
		}

		try {
			XMIParser parser = new XMIParser(file);
			System.out.println("\nThe Model is correctly formated.");

			parser.execute();

			System.out.println("\nThe Model is valid.");
		} catch (Exception e) {
			System.out.print(e.getMessage());

		}

	}
}